import Account
import asyncio
from decouple import config

client_id = config('CLIENT_ID')
client_secret = config('CLIENT_SECRET')

account = Account.NewAccount(client_id, client_secret)
account.list_credentials()
asyncio.get_event_loop().run_until_complete(account.connect())
account.list_credentials()
