import websockets
import json


class NewAccount:
    def __init__(self, client_id, client_secret):
        self.credentials = {
            'client_id': client_id,
            'client_secret': client_secret,
            'access_token': '',
            'refresh_token': '',
        }

    def list_credentials(self):
        print(self.credentials)

    async def connect(self):
        msg = {
            "jsonrpc": "2.0",
            "method": "public/auth",
            "params": {
                "grant_type": "client_credentials",
                "client_id": self.credentials['client_id'],
                "client_secret": self.credentials['client_secret']
            }
        }

        async with websockets.connect('wss://www.deribit.com/ws/api/v2') as websocket:
            await websocket.send(json.dumps(msg))
            while websocket.open:
                response = await websocket.recv()
                # do something with the response...
                data = json.loads(response)
                try:
                    self.credentials['access_token'] = data['result']['access_token']
                    self.credentials['refresh_token'] = data['result']['refresh_token']
                except KeyError:
                    print('Error when trying to connect')
                await websocket.close()
